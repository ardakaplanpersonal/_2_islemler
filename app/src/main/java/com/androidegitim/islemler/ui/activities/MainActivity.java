package com.androidegitim.islemler.ui.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidegitim.islemler.R;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class MainActivity extends Activity {

    EditText sayi1EditText;
    EditText sayi2EditText;
    Button toplaButton;
    Button cikarButton;
    TextView sonucTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewlariBagla();

        clickListenerlariAta();
    }

    private void viewlariBagla() {

        sayi1EditText = findViewById(R.id.main_edittext_ilk_sayi);
        sayi2EditText = findViewById(R.id.main_edittext_ikinci_sayi);
        toplaButton = findViewById(R.id.main_button_topla);
        cikarButton = findViewById(R.id.main_button_cikar);
        sonucTextView = findViewById(R.id.main_textview_sonuc);
    }

    private void clickListenerlariAta() {

        toplaButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                topla();
            }
        });

        cikarButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                cikar();
            }
        });
    }

    private void cikar() {

        try {

            int sayi1 = Integer.valueOf(sayi1EditText.getText().toString());

            int sayi2 = Integer.valueOf(sayi2EditText.getText().toString());

            int cikarmaSonucu = sayi1 - sayi2;

            sonucTextView.setText("Çıkarma İşlemini sonucu " + cikarmaSonucu);

        } catch (NumberFormatException e) {

            uyariGoster();
        }
    }

    private void topla() {

        try {

            int sayi1 = Integer.valueOf(sayi1EditText.getText().toString());

            int sayi2 = Integer.valueOf(sayi2EditText.getText().toString());

            int toplam = sayi1 + sayi2;

            sonucTextView.setText("Toplamı İşlemini sonucu " + toplam);

        } catch (NumberFormatException e) {

            uyariGoster();
        }
    }

    private void uyariGoster() {

        Toast.makeText(getApplicationContext(), "Lütfen sayı giriniz", Toast.LENGTH_SHORT).show();
    }
}
